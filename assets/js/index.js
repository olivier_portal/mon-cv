console.log("Bonjour Seb, Je m'apelle Olivier Portal, je suis designer produit depuis 13 ans et je viens de finir une formation afin de devenir Web Designer. Durant cette formation, je me suis rendu compte que j'adore coder même si le js ou autre php reste flou pour moi. Je le lis mais dès que je dois coder 3 lignes ....aïe!");

console.log("Pour la création de ce portfolio, je n'ai pas intégrer de js, (à part ces quelques lignes) mais j'ai coder ma feuille de style en SAAS avec la méthode BEM....et c'est la première fois pour moi! Je suis assez fier du résultat ;) même si c'est une première pour moi donc c'est possible que ça ne soit pas très propre. D'ailleurs c'est ni Mobile first ni même responsive....mais c'était ajouter des média Query ou allez à ma leçon d'aqua-poney....j'ai choisi Aqua-poney ;)");

console.log("Pour le fun, et comme je m'entraîne au js dans mon coin, je te copie colle 1 excercice que j'ai déjà fait. Merci de m'avor lu....ou pas lol");

console.log("Valider un rendez-vous, en connaissant l'heure de début du RDV et sa durée, peut-on autoriser le RDV sachant que j'ai Aqua-poney à 16h30?");

// début du rendez-vous
let debutRdvHeures = 15;
let debutRdvMinutes = 40;

console.log("l'heure de début du RDV est: " + debutRdvHeures + "h" + debutRdvMinutes);

// durée du rendez-vous
let dureeMinutes = 90;

console.log("Sa durée est de: " + dureeMinutes + " minutes");

// fin de journée
let finJourneeHeures = 16;
let finJourneeminutes = 30;

//Calcul de l'heure de fin du rendez-vous
let finRdvHeures = debutRdvHeures;
let finRdvMinutes = debutRdvMinutes + dureeMinutes;


// Transformation des minutes en heures
while(finRdvMinutes > 59){
  finRdvMinutes = finRdvMinutes - 60;
  finRdvHeures = finRdvHeures + 1;
}

// Est-ce que la fin du RDV est avant la fin de la journée ?
if (finRdvHeures < finJourneeHeures) {
    console.log("RDV OK");
  } else if (finRdvHeures === finJourneeHeures && finRdvMinutes <= finJourneeminutes){
    console.log("RDV OK");
  } else {
    console.log("RDV impossible");
  }

  if(finRdvMinutes < 10){
    console.log("Le RDV se terminera à " + finRdvHeures + "h0" + finRdvMinutes);
  } else {
    console.log("Le RDV se terminera à " + finRdvHeures + "h" + finRdvMinutes);
  }